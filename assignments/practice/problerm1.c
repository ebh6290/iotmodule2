/*code and explanations by EB HRISHIKESH*/
/*include the required headerfiles*/
#include <stdio.h>   // for Standard Input Output
#include <stdlib.h>  // for memory allocation, process control,conversions and others
#include <string.h> // for manipulating C strings and arrays

/** Network Header files */
/** we will use this header files
    to fetch IP address of the device */
#include <netdb.h>    // for network database operations.
#include <ifaddrs.h> // for getting network interface addresses.

/** Shunya interfaces header file*/
/** We will use this header file to send our JSON
    message to the AWS cloud using Shunya interfaces APIs */
#include <shunyaInterfaces.h> // to program embedded boards to communicate with PLC's, Sensors, Actuators and Cloud

/** Time Header file*/
/** We will use this header file to fetch timestamp */
#include <time.h> // to get and manipulate date and time information

/** MQTT protocol Header file*/
/** We will use this header file to make MQTT message payload*/
#include "MQTTClient.h" // contains APIs to send/receive MQTT data

/** information about the function which is used below 

    * This is function will fetch a data of the device
    * This data contains a parameters which will be stored in a JSON format
    * This JSON message contains id of device, timestamp, type of message and an IP address of the network connected.
    * first id of the device which is stored in "/etc/shunya/deviceid" will be fetched.
    * Then UNIX timestamp fetched through API from time.h header file.
    * After that type of event which will be "heartbeat" message.
    * At the end we will fetch an IP address of the network at which our device is connected
    * void is a datatype that does not return any value
    
*/
/*code*/
void getdevicedata(void)
{
// Fetches ID of the device
FILE *idl;
char id[255];
idl = fopen("/etc/shunya/deviceid", "r");
while(fscanf(idl, "%s", id)!=EOF)
{printf("%s ", id );}

// Fetches timestamp
int stp = (gettimeofday(2));

// Fetches IP address
void checkIPbuffer(char *ip)
{if (NULL == ip)
{perror("inet_ntoa");
exit(1);}}
char *ip;
ip = inet_ntoa(*((struct in_addr*)
host_entry->h_addr_list[0]));}


/** information about the function which is used below 

    * This function will configure connection with AWS ioT core cloud
    * Here are some parameters upon which AWS will configure a connection with it's MQTT broker
        - Endpoint: it is an URL at which our network will connect.
        - Port: for AWS it is by default 8833.
        - Certificate directory: this contains directory of the certificates of AWS ioT core.
        - Root certificate: a root certificate is a public key certificate that identifies a root certificate authority.
        - Client certificate: a client certificate is a type of digital certificate,
        that is used by client systems to make authenticated requests to a remote server.
        - Private key: It is stored on user's device and is used to decrypt data.
        - Client ID: It is a special ID given to an user.
    * void is a datatype that does not return any value
    
*/
/*code*/
void AWS_config(void)
{"user": {
"endpoint": " ",
"port": 8883,
"certificate dir": "/home/shunya/.cert/aws/",
"root certificate": " ",
"client certificate": " ",
"private key": " ",
"client ID": " "}}


/** information about the function which is used below 
    * This function will send a JSON message to an AWS ioT core MQTT broker.
    * First it will make a message named heartbeat by MQTTClient message initializer.
    * Then create new AWS instance, And connect to that instance.
    * Then it create a payload of JSON message.
    * At this point all the values of the device are fetched through getdevicedata function.
    * Using APIs payload will be published to "device/heartbeat" topic.
    * At the end connection should be released from MQTT broker for completing a process.
    * void is a datatype that does not return any value
    
*/
/*code*/
void send_data()
{MQTTClient_message heartbeat = MQTTClient_message_initializer; //initializes message to be sent on MQTT broker
awsObj user = newAws("user");                                   //creates new instance with AWS
awsConnectMqtt(&user);                                          //connects to the MQTT broker of AWS ioT core
heartbeat.payload("device":{
"deviceId": id,
"timestamp": stp,
"eventType": "heartbeat",
"ipAddress": ip
}.getBytes());                                                  //creates a payload of the message
awsPublishMqtt(&user, "device/heartbeat", "%s" ,heartbeat);    //publishes a payload on AWS MQTT broker
awsDisconnectMqtt(&user);                                     //releases connection with MQTT broker
}

/** information about the main function
   * This function will call all the functions made above and completes whole process.
    * First it will call shunyaInterfacesSetup function this will initiates APIs contained in shunya interfaces library.
    * Then it will fetch data of the device by calling getdevicedata function.
    * In order to publish the data it should be connected to MQTT broker this step is done by calling AWS_config function.
    * Finally by calling send_data function it will publish the data on particular topic.
    * void is a datatype that does not return any value
    
*/
/*code*/
int main (void)
{
shunyaInterfacesSetup () ; // function to initiate shunya interfaces
getdevicedata();          // function to get data of the device (device id, timestamp, event type, IP address
AWS_config();            // function to configure connection with AWS MQTT broker
send_data();            // function to publish data on AWS MQTT broker
}
